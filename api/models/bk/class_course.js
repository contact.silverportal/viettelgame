/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const class_course = sequelize.define('class_course', {
    'id': {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    'name': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'desc': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'gradeId': {
      type: DataTypes.STRING(50),
      references: {
        model: 'class_grade',
        key: 'id',
        as: 'gradeId'
      }
    }
  }, {
    tableName: 'class_course'
  });

  class_course.associate = (models) => {
    class_course.belongsTo(models.class_grade, {
      foreignKey: {
        name: 'gradeId',
        allowNull : false
      },
      as: 'grade'
    });
  };

  return class_course;
};
