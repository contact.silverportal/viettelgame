/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exam', {
    'id': {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    'exam_type_id': {
      type: DataTypes.INTEGER(10),
    },
    'name': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'date': {
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'exam'
  });
};
