/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exam_type', {
    'id': {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    'name': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'decs': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'exam_type'
  });
};
