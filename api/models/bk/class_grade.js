/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const class_grade = sequelize.define('class_grade', {
    'id': {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    'name': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'desc': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'class_grade'
  });

  class_grade.associate = (models) => {
    class_grade.hasMany(models.class_course, {
      foreignKey: {
        name: 'gradeId',
        allowNull : false
      },
      as: 'course'
    });
  };

  return class_grade;
};
