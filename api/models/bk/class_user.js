/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('class_user', {
    'id': {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
	  'classid': {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
	  'userid': {
      type: DataTypes.INTEGER(10),
      allowNull: true,
    },
	'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'class_user'
  });
};
