/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('class_info', {
    'id': {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    'name': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'teacherid': {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    'gradeid': {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    'status': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'year': {
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'class_info'
  });
};
