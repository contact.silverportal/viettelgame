/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exam_result', {
    'id': {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    'examid': {
      type: DataTypes.INTEGER(10)
    },
    'userid': {
      type: DataTypes.INTEGER(10)
    },
    'courseid': {
      type: DataTypes.INTEGER(10)
    },
    'marks': {
      type: DataTypes.STRING(50)
    },
    'thumb': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'etc': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'exam_result'
  });
};
