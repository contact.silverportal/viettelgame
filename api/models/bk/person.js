/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person', {
    'id': {
      type: DataTypes.STRING(50),
      primaryKey: true
    },
    'fullname': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'email': {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    'phone': {
      type: DataTypes.TEXT,
      defaultValue: ''
    },
    'dob': {
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW
    },
	  'active': {
      type: DataTypes.BOOLEAN,
      defaultValue: 0
    },
    'createdAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'person'
  });
};
