/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('track_game_002', {
    'id': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'uid': {
      type: DataTypes.STRING(45),
      allowNull: true,
      comment: "null"
    },
    'session': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'event': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'time': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'createdAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'track_game_002'
  });
};
