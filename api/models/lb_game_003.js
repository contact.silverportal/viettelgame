/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lb_game_003', {
    'ID': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'UserID': {
      type: DataTypes.STRING(12),
      allowNull: true,
      comment: "null"
    },
    'Name': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'Score': {
      type: DataTypes.DECIMAL,
      allowNull: true,
      comment: "null"
    },
    'Data': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'createdAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'lb_game_003'
  });
};
