/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    'ID': {
      type: DataTypes.STRING(12),
      allowNull: false,
      primaryKey: true,
      comment: "null"
    },
    'Name': {
      type: DataTypes.STRING(45),
      allowNull: true,
      comment: "null"
    },
    'Phone': {
      type: DataTypes.STRING(45),
      allowNull: true,
      comment: "null"
    },
    'Email': {
      type: DataTypes.STRING(45),
      allowNull: true,
      comment: "null"
    },
    'UA': {
      type: DataTypes.STRING(45),
      allowNull: true,
      comment: "null"
    },
    'createdAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'user'
  });
};
