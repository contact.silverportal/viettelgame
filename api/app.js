const createError = require("http-errors");
const express = require("express");
const cors = require('cors');
const fs = require('fs');
var favicon = require('serve-favicon');
const path = require("path");
const cookieParser = require("cookie-parser");
const api = require("./api");
const bodyParser = require("body-parser");
const omitEmpty = require('omit-empty');
const app = express();

app.use(cors());
app.use(express.static('public'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/public',express.static(path.join(__dirname, "public")));
app.use('/static',express.static(path.join(__dirname, "static")));
app.get('/favicon.ico', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, "static") + '/favicon.ico'));
});
app.get('/001', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, "static") + '/001/index.html'));
});
app.get('/002', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, "static") + '/002/index.html'));
});
app.get('/003', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, "static") + '/003/index.html'));
});

app.get('/log', (req, res) => {
  res.sendFile(path.join(path.join(__dirname + '/log.txt')));
});

function removeEmptyProperties(req, res, next) {
  req.body = omitEmpty(req.body);
  req.params = omitEmpty(req.params);
  req.query = omitEmpty(req.query);
  next();
}

function logResponseBody(req, res, next) {
  var _logFile = fs.readFileSync('log.txt', 'utf8');
  var _arrLogs = [];
  if (_logFile != '') _arrLogs = JSON.parse(_logFile);
  var _shoudAllow = ['/',"/public","/favicon.ico"];
  console.log(req.path);
  for (var i in _shoudAllow){
    if (req.path == _shoudAllow[i]){
      next();
    }
  }
  var oldWrite = res.write,
      oldEnd = res.end;
  var chunks = [];
  try{
    res.write = function (chunk) {
      chunks.push(chunk);
      oldWrite.apply(res, arguments);
    };
    res.end = function (chunk) {
      if (chunk) chunks.push(chunk);
      try{
        var body = Buffer.concat(chunks).toString('utf8');
      } catch(err) {
        var body = {};
      }
      
      let _objLog = {
        method : req.method,
        param : req.query,
        url : req.path,
        body : req.body,
        header : req.headers,
        status : res.statusCode,
        res : body
      };
      _arrLogs.unshift(_objLog);
      
      const _rm = ['accept','cache-control','content-length','user-agent','postman-token','host','accept-encoding','connection'];
      for (var i in _rm){
        delete _objLog.header[_rm[i]];
      }
      fs.writeFileSync('log.txt', JSON.stringify(_arrLogs));
      oldEnd.apply(res, arguments);
    };
  }
  catch(err) {

  }

  next();
  
}
app.use(removeEmptyProperties);
app.use(logResponseBody);
app.use(api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;


/***
FgBlack = "\x1b[30m"
FgRed = "\x1b[31m"
FgGreen = "\x1b[32m"
FgYellow = "\x1b[33m"
FgBlue = "\x1b[34m"
FgMagenta = "\x1b[35m"
FgCyan = "\x1b[36m"
FgWhite = "\x1b[37m"

BgBlack = "\x1b[40m"
BgRed = "\x1b[41m"
BgGreen = "\x1b[42m"
BgYellow = "\x1b[43m"
BgBlue = "\x1b[44m"
BgMagenta = "\x1b[45m"
BgCyan = "\x1b[46m"
BgWhite = "\x1b[47m"
***/
/*
[
  [ 'warn',  '\x1b[33m' ],
  [ 'error', '\x1b[31m' ],
  [ 'log',   '\x1b[2m'  ]
].forEach(function(pair) {
  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1] + '\n';
  console[method] = console[method].bind(console, color, method.toUpperCase(), reset);
});
*/