const JwtStrategy = require("passport-jwt").Strategy;
const jwtConfig = require("../config/jwtConfig");
const User = require("../models").user;

module.exports = new JwtStrategy(jwtConfig, async (jwt_payload, done) => {
  const userID = jwt_payload.id;
  let user = await User.findOne({ where: { id : userID} });
  if (user) {
    return done(null, user);
  } else {
    return done(null, false);
  }
});
