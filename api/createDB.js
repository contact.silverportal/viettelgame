"use strict";
var uuid = require('random-uuid-v4');
const path = require("path");
const Sequelize = require("sequelize");
//const env = process.env.NODE_ENV || "development";
const env = "development";
const config = require(__dirname + "/config/config.json")[env];
const db = {};

console.log("## RUN AS ENV : "+env);
try{
  let sequelize;
  if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(
      config.database,
      config.username,
      config.password,
      config
    );
  }
  sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");

    

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;
    db.user = require("./models/user")(sequelize, Sequelize);
    db.user = require("./models/lb_game_001")(sequelize, Sequelize);
    db.user = require("./models/lb_game_002")(sequelize, Sequelize);
    db.user = require("./models/lb_game_003")(sequelize, Sequelize);
    sequelize.sync({
        force: true,
        logging: (t) => {
            console.log(t);
        }
    }).then(() =>{
        console.log("## DONE");
        const _uid = uuid();
        console.log(_uid);
    });
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

  
}
catch(e){
  console.log('database is not connected !!!');
  console.log(e);
  return;
}


module.exports = db;
