var express = require("express");
var router = express.Router();

const userController = require("../controllers/user");
const leaderboardController = require("../controllers/leaderboard");
const passport = require("passport");
const jwtStrategry = require("../strategies/jwt");
const multer = require('multer');
const path = require("path");
passport.use(jwtStrategry);

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/avatar');
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
  }
});
var upload = multer({ storage: storage });

function authenticateJwt(req, res, next) {
  passport.authenticate('jwt', function(err, user, info) {
    if (err) return next(err);
    if (!user) {
      return res.status(401).send({
        "code": -1,
        "mess": "Không thể xác thực người dùng"
      });
    }
    req.user = user;
    next();
  })(req, res, next);
}

router.post("/api/:id", function(req, res) {
  userController.processData(req, res);
});

router.post("/api/lb/:id", function(req, res) {
  userController.getLB(req, res);
});

router.use(function(err, req, res, next) {
  if (err.name === "ValidationError") {
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key) {
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;
