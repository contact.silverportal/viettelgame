var uuid = require('random-uuid-v4');
const models = require("../models");
const logs = require("./log").logs;
const User = models.user;
const Person = models.person;
const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");
const availableUserRoles = require("../config/userRolesConfig");

exports.addUser = async function(req, res) {
  const { pass , n_pass , name} = req.body;
  console.log(pass +" "+ n_pass +" "+ name);

  if ((!pass) || (!n_pass) || (!name)){
    return res.status(401).send({
      code : 0,
      mess : logs.nofill
    });
  }
  if (pass !== n_pass) {
    return res.status(401).send({
      code : 0,
      mess : logs.passxpass
    });
  }
  if (pass.length < 4 || pass.length > 20) {
    return res.status(401).send({
      code : 0,
      mess : logs.invalidPass
    });
  }
  let isExisted = await exports.isExisted(name);
  if (isExisted){
    return res.status(401).send({
      code : 0,
      mess : logs.duplicate
    });
  }
  const _uid = uuid();

  const dbUser = await User.create({
      id : _uid,
      name: name,
      password: pass,
      role: '0'
  });
  const dbPerson = await Person.create({
    id : _uid,
    fullname: name
  });
  if (dbUser && dbPerson){
    const token = jwt.sign(
      {
        exp: Math.floor(Date.now() / 1000) + 60 * 60,
        id: dbUser.id,
        name: dbUser.name,
        role: dbUser.role
      },
      jwtConfig.secretOrKey
    );
    return res.send({
      code : 100,
      data: {
        user : {
          name: name,
          avatar : '',
          role : 0
        },
        token : token
      }
    });
  } else {
    return res.status(401).send({
      code : -1,
      mess : logs.common
    });
  }
};

exports.authenticate = async function authenticate(req, res, next) {
  const { pass , name} = req.body;
  if (!pass || !name) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
  try {
    const dbUser = await User.findOne({ where: { name } });
    let correctCredentials = (pass == dbUser.password);
    if (correctCredentials) {
      const token = jwt.sign(
        {
          exp: Math.floor(Date.now() / 1000) + 60 * 600,
          id: dbUser.id,
          name: dbUser.name,
          role: dbUser.role
        },
        jwtConfig.secretOrKey
      );
      return res.send({
        code : 100,
        data: {
          id : dbUser.id,
          name: dbUser.name,
          avatar : dbUser.avatar,
          token : token
        }
      });
    } else {
      return res.status(401).send({
        code : 0,
        mess : logs.failLogin
      });
    }
  } catch (e) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
};

exports.isAdmin = async function(userId) {
  /*
  return await User.findOne({
    where: {
      id: userId,
      role: "administrator"
    }
  });
  */
};

exports.isExisted = async function(username) {
  return await User.findOne({
    where: {
      name: username,
    }
  });
};

exports.getUser = async function(req, res) {
  const userId = req.params.id;
  if (req.user.id != req.params.id || req.user.role < 0){
    res.status(400).send({
      code : -1,
      mess : logs.norole
    });
  }
  let user = await User.findOne({

    }
    ,{
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin người dùng thành công',
      data : user
    });
  }
};

exports.getUsers = async function(req, res) {
  let user = await User.findAll();
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin người dùng thành công',
      data : user
    });
  }
};

exports.deleteUser = async function(req, res) {
  try {
    User.destroy({
      where: {
        id: userId
      }
    });

    return res.send("user deleted");
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
};

exports.updateUser = async function(req, res) {
  const userId = req.user.id;
  const { phone , email } = req.body;
  if ((!phone) || (!email)){
    res.status(200).send({
      code : 0,
      mess : 'Dữ liệu yêu cầu không được để trống'
    });
  }
  let user = await User.update({ phone: phone, email : email }, {
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  } else {
    res.status(200).send({
      code : 100,
      mess : 'Update thông tin thành công'
    });
  }
};


exports.updateAvatar = async function(req, res) {
  const userId = req.user.id;
  console.log("user ID: "+userId);
  const _file = '/public/uploads/avatar/'+req.file.filename;
  console.log("avatar : "+_file);
  let user = await User.update({ avatar: _file }, {
    where: {
      id : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Ảnh đại diện đã được cập nhật'
    });
  }
};

exports.getLB = async function(req, res) {
  const id = req.params.id;
  let data;
  console.log(id);
  if (id == "001"){
    data = await lb_game_001.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
  if (id == "002"){
    data = await lb_game_002.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
  if (id == "003"){
    data = await lb_game_003.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
};