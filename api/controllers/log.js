module.exports.logs = {
    'nofill'      : 'Dữ liệu yêu cầu không được để trống',
    'passxpass'   : 'Mật khẩu và xác nhận mật khẩu không trùng khớp',
    'invalidPass' : 'Mật khẩu phải bao gồm từ 4 đến 20 ký tự',
    'duplicate'   : 'Dữ liệu yêu cầu đã tồn tại',
    'common'      : 'Hệ thống gặp sự cố, vui lòng thử lại sau',
    'failLogin'   : 'Tên đăng nhập hoặc mật khẩu không đúng',
    'norole'      : 'Dữ liệu yêu cầu không được cấp quyền'
};
  