const models = require("../models");
const { Op } = require("sequelize");
const logs = require("./log").logs;
const lb_game_001 = models.lb_game_001;
const lb_game_002 = models.lb_game_002;
const lb_game_003 = models.lb_game_003;
const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");

exports.add = async function(req, res , next) {
  const { id , score , data} = req.body;
  const user = req.user;
  if (!id){
	return res.status(401).send({
      code : -1,
      mess : logs.common
    });
  }
  if (id == "001"){
	  const isExisted = await lb_game_001.findOne({
      where: {
        UserID : user.ID
      }
    });
    if (!isExisted){
      const createNew = await lb_game_001.create({
          UserID : user.ID,
          Name : user.Name,
          Score : score,
          Data : data
      });
      if (createNew)
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    } else {
      if (isExisted.Score < score){
        const updateScore = await lb_game_001.update({ Name: user.Name , Score : score , Data : data }, {
          where: {
            UserID : user.ID
          }
        });
      }
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    }
  }
  if (id == "002"){
	  const isExisted = await lb_game_002.findOne({
      where: {
        UserID : user.ID
      }
    });
    if (!isExisted){
      const createNew = await lb_game_002.create({
          UserID : user.ID,
          Name : user.Name,
          Score : score,
          Data : data
      });
      if (createNew)
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    } else {
      if (isExisted.Score < score){
        const updateScore = await lb_game_002.update({ Name: user.Name , Score : score , Data : data }, {
          where: {
            UserID : user.ID
          }
        });
      }
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    }
  }
  if (id == "003"){
	  const isExisted = await lb_game_003.findOne({
      where: {
        UserID : user.ID
      }
    });
    if (!isExisted){
      const createNew = await lb_game_003.create({
          UserID : user.ID,
          Name : user.Name,
          Score : score,
          Data : data
      });
      if (createNew)
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    } else {
      if (isExisted.Score < score){
        const updateScore = await lb_game_003.update({ Name: user.Name , Score : score , Data : data }, {
          where: {
            UserID : user.ID
          }
        });
      }
      return res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    }
  }
  return res.status(401).send({
    code : -1,
    mess : logs.common
  });
};

exports.get = async function(req, res) {
  const id = req.params.id;
  let data;
  console.log(id);
  if (id == "001"){
    data = await lb_game_001.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
  if (id == "002"){
    data = await lb_game_002.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
  if (id == "003"){
    data = await lb_game_003.findAll({
      where: {
        Name : {
          [Op.ne]: null
        }
      },
      attributes: ['Name','Score','Data'],
      order: [
        ['Score', 'DESC']
      ]
    });
    return res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin thành công',
      data : data
    });
  }
};