const models = require("../models");
const logs = require("./log").logs;
const { Op } = require("sequelize");
const User = models.user;
const Track = [
  null,
  models.track_game_001,
  models.track_game_002,
  models.track_game_003
];
const LB = [
  null,
  models.lb_game_001,
  models.lb_game_002,
  models.lb_game_003
];
const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");
//const availableUserRoles = require("../config/userRolesConfig");

exports.addUser = async function(req, res , next) {
  const { id } = req.body;
  let isExisted = await exports.isExisted(id);
  if (isExisted){
    await exports.authenticate(req, res, next);
  }
  const dbUser = await User.create({
      ID : id
  });

  if (dbUser){
    const token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + 60 * 60,
        id: dbUser.ID,
        name: dbUser.Name
      },
      jwtConfig.secretOrKey
    );
    return res.send({
      code : 100,
      data: {
        user : {
          id : dbUser.ID,
          name: dbUser.Name
        },
        token : token
      }
    });
  } else {
    return res.status(401).send({
      code : -1,
      mess : logs.common
    });
  }
};

exports.authenticate = async function authenticate(req, res, next) {
  const { id } = req.body;
  if (!id) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
  try {
    const dbUser = await User.findOne({ where: { ID: id } });
    let correctCredentials = true;
    if (correctCredentials) {
      const token = jwt.sign(
        {
          exp: Math.floor(Date.now() / 1000) + 60 * 600,
          id: dbUser.ID,
          name: dbUser.Name
        },
        jwtConfig.secretOrKey
      );
      return res.send({
        code : 100,
        data: {
          id : dbUser.ID,
          name: dbUser.Name,
          token : token
        }
      });
    } else {
      return res.status(401).send({
        code : 0,
        mess : logs.failLogin
      });
    }
  } catch (e) {
    return res.status(401).send({
      code : 0,
      mess : logs.failLogin
    });
  }
};

exports.isAdmin = async function(userId) {
  /*
  return await User.findOne({
    where: {
      id: userId,
      role: "administrator"
    }
  });
  */
};

exports.isExisted = async function(id) {
  return await User.findOne({
    where: {
      ID: id
    }
  });
};

exports.getUser = async function(req, res) {
  const userId = req.params.id;
  if (req.user.id != req.params.id || req.user.role < 0){
    res.status(400).send({
      code : -1,
      mess : logs.norole
    });
  }
  let user = await User.findOne({

    }
    ,{
    where: {
      ID : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin người dùng thành công',
      data : user
    });
  }
};

exports.getUsers = async function(req, res) {
  let user = await User.findAll();
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  }
  else {
    res.status(200).send({
      code : 100,
      mess : 'Lấy thông tin người dùng thành công',
      data : user
    });
  }
};

exports.deleteUser = async function(req, res) {
  try {
    User.destroy({
      where: {
        ID: userId
      }
    });

    return res.send("user deleted");
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
};

exports.updateUser = async function(req, res) {
  const userId = req.user.ID;
  const { name, phone , email } = req.body;
  if (!name){
    res.status(200).send({
      code : 0,
      mess : 'Dữ liệu yêu cầu không được để trống'
    });
  }
  let user = await User.update({ Name: name , Phone : phone , Email : email }, {
    where: {
      ID : userId
    }
  });
  if (!user) {
    res.status(400).send({
      code : -1,
      mess : ''
    });
  } else {
    res.status(200).send({
      code : 100,
      mess : 'Update thông tin thành công'
    });
  }
};


exports.processData = async function(req, res) {
  const gameId = parseInt(req.params.id);
  const { data } = req.body;
  var _req = false;
  var _updateLB = async function(_data){
    const isExisted = await LB[gameId].findOne({
      where: {
        UserID : _data.id
      }
    });
    if (!isExisted){
      const createNew = await LB[gameId].create({
          UserID : _data.id,
          Name : _data.name,
          Score : _data.score,
          Data : _data.data
      });
    } else {
      if (isExisted.Score < _data.score){
        const updateScore = await LB[gameId].update({ Name: _data.name , Score : _data.score , Data : _data.data }, {
          where: {
            UserID : _data.id
          }
        });
      }
    }
  };
  var _addData = async function(_data){
    log(_data);
    var _obj = _data.split(",");
    console.log(_obj)
    for (var i in _obj){
      var _arr = _obj[i].split('||');
      var _new = {
        uid : _arr[0],
        name : _arr[1],
        session : _arr[2],
        event : _arr[3],
        data : _arr[4],
        time : _arr[5]
      };
      if (_new.event == "finish"){
        var _gameData = _new.data.split("+");
        const _lbObj = {id : _new.uid, name : _new.name , score : _gameData[0], data : _gameData[1]};
        _req = await _updateLB(_lbObj);
      }
      _req = await Track[gameId].create(_new);
    }
    if (!_req) {
      res.status(400).send({
        code : -1,
        mess : ''
      });
    } else {
      res.status(200).send({
        code : 100,
        mess : 'Update thông tin thành công'
      });
    }
  };
  _addData(data);
};

exports.getLB = async function(req, res) {
  const gameId = req.params.id;
  let data;
  data = await LB[gameId].findAll({
    where: {
      Name : {
        [Op.ne]: null
      }
    },
    attributes: ['Name','Score','Data'],
    order: [
      ['Score', 'DESC']
    ]
  });
  return res.status(200).send({
    code : 100,
    mess : 'Lấy thông tin thành công',
    data : data
  });
};

function log(_log){
  console.log(_log);
}